adj(X, Y):- edge(X, Y, _); edge(Y, X, _).
path(X, Y, P):- path1(X, P, [Y]).
path1(X, [X|T], [X|T]).
path1(X, P, [H|T]):- adj(N, H), (\+ member(N, [H|T])), path1(X, P, [N, H|T]).

paths(X, Y, PP):- findall(P, path(X, Y, P), PP).

edge(0, 1, 10).
edge(1, 7, 20).
edge(0, 2, 3).
edge(2, 4, 4).
edge(4, 7, 5).
edge(0, 3, 1).
edge(3, 5, 1).
edge(5, 6, 2).
edge(6, 7, 1).

$ ������ �� �����(