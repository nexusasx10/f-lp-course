scanAcc([], Acc, Acc).
scanAcc([H|T], R, Acc):- isList(H) -> scan(H, R1), reverse(R1, RR1), append(RR1, Acc, R2), scanAcc(T, R, R2); scanAcc(T, R, [H|Acc]).
scan(L, R1):- scanAcc(L, R, []), reverse(R, R1).

isList([]).
isList([_|_]).