correct_state(MA, CA, MB, CB):-
  MA >= 0, CA >= 0, MB >= 0, CB >= 0,
  (MA >= CA; MA == 0), (MB >= CB; MB == 0).

step(MAS, CAS, MBS, CBS, a, MAE, CAS, MBE, CBS, b):- MAE is MAS - 1, MBE is MBS + 1.
step(MAS, CAS, MBS, CBS, a, MAE, CAS, MBE, CBS, b):- MAE is MAS - 2, MBE is MBS + 2.
step(MAS, CAS, MBS, CBS, a, MAS, CAE, MBS, CBE, b):- CAE is CAS - 1, CBE is CBS + 1.
step(MAS, CAS, MBS, CBS, a, MAS, CAE, MBS, CBE, b):- CAE is CAS - 2, CBE is CBS + 2.
step(MAS, CAS, MBS, CBS, a, MAE, CAE, MBE, CBE, b):- MAE is MAS - 1, MBE is MBS + 1, CAE is CAS - 1, CBE is CBS + 1.
step(MAS, CAS, MBS, CBS, b, MAE, CAS, MBE, CBS, a):- MAE is MAS + 1, MBE is MBS - 1.
step(MAS, CAS, MBS, CBS, b, MAE, CAS, MBE, CBS, a):- MAE is MAS + 2, MBE is MBS - 2.
step(MAS, CAS, MBS, CBS, b, MAS, CAE, MBS, CBE, a):- CAE is CAS + 1, CBE is CBS - 1.
step(MAS, CAS, MBS, CBS, b, MAS, CAE, MBS, CBE, a):- CAE is CAS + 2, CBE is CBS - 2.
step(MAS, CAS, MBS, CBS, b, MAE, CAE, MBE, CBE, a):- MAE is MAS + 1, MBE is MBS - 1, CAE is CAS + 1, CBE is CBS - 1.

correct_step(MAS, CAS, MBS, CBS, PS, MAE, CAE, MBE, CBE, PE):-
  step(MAS, CAS, MBS, CBS, PS, MAE, CAE, MBE, CBE, PE),
  correct_state(MAE, CAE, MBE, CBE).

solution(M, C, Steps):- solution_builder(M, C, 0, 0, a, [[M, C, 0, 0, a]], Res), reverse(Res, Steps).
solution_builder(MAS, CAS, MBS, CBS, PS, Acc, Res):-
  correct_step(MAS, CAS, MBS, CBS, PS, MAE, CAE, MBE, CBE, PE),
  (\+ member([MAE, CAE, MBE, CBE, PE], Acc)),
  solution_builder(MAE, CAE, MBE, CBE, PE, [[MAE, CAE, MBE, CBE, PE]|Acc], Res).
solution_builder(0, 0, _, _, b, Acc, Acc).

min_solution(M, C, Res1):- findall(Sol, solution(M, C, Sol), Res), min_len_list(Res, Res1).

min_len_list([H|T], Res):- length(H, N), min_len_list_finder(T, N, H, Res).
min_len_list_finder([H|T], Min_L, Val, Res):- length(H, L),
  (L < Min_L -> min_len_list_finder(T, L, H, Res) ; min_len_list_finder(T, Min_L, Val, Res)).
min_len_list_finder([], _, Val, Val).

print_solution(M, C):- min_solution(M, C, Res), print_solution_stage(Res).
print_solution_stage([H|T]):- print_state(H), print_solution_stage(T).
print_solution_stage([]).

print_state([MA, CA, MB, CB, P]):-
  format('M-%u, C-%u ', [MA, CA]),
  (P == a -> write('|>>  |') ; write('|  <<|')),
  format(' M-%u, C-%u\n', [MB, CB]).
