:- include('graph.pro').

adjacent(A, B):- edge(A, B); edge(B, A).

path(A, B, Res):- pathBuilder(A, Res, [B]).
pathBuilder(A, Res, [H|T]):- adjacent(N, H), \+ member(N, [H|T]), pathBuilder(A, Res, [N, H|T]).
pathBuilder(A, [A|T], [A|T]).

component(A, Res):- node(A), findall(B, path(A, B, _), Res).

components(Comps):- setof(SComp, A^Comp^(component(A, Comp), sort(Comp, SComp)), Comps).

set(List, Set):- setBuilder(List, Set, []).
setBuilder([H|T], Set, Acc):- member(H, T) -> setBuilder(T, Set, Acc); setBuilder(T, Set, [H|Acc]).
setBuilder([], Acc, Acc).
