{-# OPTIONS_GHC -XDeriveDataTypeable -XScopedTypeVariables #-}

import Data.Typeable
import Control.Exception
import System.Environment


data ArgException = ArgException deriving(Typeable)
instance Show ArgException where
  show _ = "usage: countLines filename"
instance Exception ArgException

getCmdArg = do
  args <- getArgs
  if length args /= 1 then
    throw ArgException
  else
    return $head$args

processFile = do
  filename <- getCmdArg
  s <- readFile filename
  print $length$lines s

main = processFile `catches`
  [Handler(\(ex::IOException)->putStrln$show ex),
   Handler(\(ex::SomeException)->putStrln$show ex)]

{-- bracket f1 f2 f3
fileSize path = handle (...) $ do
  bracket (openFile path ReadMode) hClose $
    ...
--}
