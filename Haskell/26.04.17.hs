type MyVector a =(a, a)  -- псевдоним

myVectorSum::(Num a) => MyVector a -> MyVector a -> MyVector a
myVectorSum (a, b) (c, d)=(a+c, b+d)

-- type Name = String
-- type Work = String

-- showInfo::Name->Work->String
-- showInfo name work = "Name: " ++ name ++ "\nWork: " ++ work

data Name = Name String
data Work = Work String

showInfo::Name->Work->String
showInfo (Name name) (Work work) = "Name: " ++ name ++ "\nWork: " ++ work

name::Name
name = Name "Robin"
work::Work
work = Work "Coder"

display = putStrLn $ showInfo name work

data Complex a = Complex a a

inv::(Num a) => Complex a -> Complex a
inv (Complex x y) = Complex x (-y)

myPrintComplex::(Num a, Show a) => Complex a -> String
myPrintComplex (Complex x, y)
  | y > 0 = show x ++ " + " ++ show y ++ "i"
  | otherwise = show x ++ " " ++ show y ++ "i"

-- Complex {real=3, imag=4}


data MultiTree a = Leaf | Node a [MultiTree a]

countInNode::MultiTree a -> Int
countInNode = 
