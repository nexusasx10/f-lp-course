module RedBlackTree(
  RBTree(..), Color(..),
  size, color, height, append, delete, contains, from_list
) where


data Color = R | B deriving(Show, Eq)

data RBTree t = Empty | Node Color (RBTree t) t (RBTree t)

instance (Show t) => Show (RBTree t) where
  show Empty = "Empty"
  show (Node c l n r) =
    unlines (show_tree (Node c l n r))
      where
        pad :: String -> String -> [String] -> [String]
        pad first rest = zipWith (++) (first : repeat rest)

        show_subtrees :: (Show t) => RBTree t -> RBTree t -> [String]
        show_subtrees l r =
          pad "+- " "|  " (show_tree l) ++ pad "`- " "   " (show_tree r)

        show_tree :: (Show t) => RBTree t -> [String]
        show_tree Empty = []
        show_tree (Node c l n r) =
          ("(" ++ show n ++ "," ++ show c ++ ")") : show_subtrees l r

from_list :: (Ord t) => [t] -> RBTree t
from_list l = _from_list Empty l where
  _from_list :: (Ord t) => RBTree t -> [t] -> RBTree t
  _from_list t (h:tl) = _from_list (append h t) tl
  _from_list t [] = t

size :: RBTree t -> Int
size Empty = 0
size (Node _ l _ r) = size l + size r + 1

color :: RBTree t -> Color
color Empty = B
color (Node c _ _ _) = c

height :: RBTree t -> Int
height Empty = 0
height (Node _ l _ r) = (max (height l) (height r)) + 1

contains :: (Ord t) => t -> RBTree t -> Bool
contains _ Empty = False
contains e (Node _ l v r)
  | e < v = contains e l
  | e > v = contains e r
  | otherwise = True

append :: (Ord t) => t -> RBTree t -> RBTree t
append x s = Node B a z b where
  Node _ a z b = _append s
  _append Empty = Node R Empty x Empty
  _append s@(Node B a y b)
    | x < y = balance_a (_append a) y b
    | x > y = balance_a a y (_append b)
    | otherwise = s
  _append s@(Node R a y b)
    | x < y = Node R (_append a) y b
    | x > y = Node R a y (_append b)
    | otherwise = s

balance_a :: RBTree t -> t -> RBTree t -> RBTree t
balance_a (Node R a x b) y (Node R c z d) = Node R (Node B a x b) y (Node B c z d)
balance_a (Node R (Node R a x b) y c) z d = Node R (Node B a x b) y (Node B c z d)
balance_a (Node R a x (Node R b y c)) z d = Node R (Node B a x b) y (Node B c z d)
balance_a a x (Node R b y (Node R c z d)) = Node R (Node B a x b) y (Node B c z d)
balance_a a x (Node R (Node R b y c) z d) = Node R (Node B a x b) y (Node B c z d)
balance_a a x b = Node B a x b

delete :: (Ord t) => t -> RBTree t -> RBTree t
delete x t = case _delete t of {Node _ a y b -> Node B a y b; _ -> Empty}
  where
  _delete Empty = Empty
  _delete (Node _ a y b)
      | x < y = del_from_left a y b
      | x > y = del_from_right a y b
      | otherwise = move_subtree a b
  del_from_left a@(Node B _ _ _) y b = balance_d_l (_delete a) y b
  del_from_left a y b = Node R (_delete a) y b
  del_from_right a y b@(Node B _ _ _) = balance_d_r a y (_delete b)
  del_from_right a y b = Node R a y (_delete b)

balance_d_l :: RBTree t -> t -> RBTree t -> RBTree t
balance_d_l (Node R a x b) y c = Node R (Node B a x b) y c
balance_d_l bl x (Node B a y b) = balance_a bl x (Node R a y b)
balance_d_l bl x (Node R (Node B a y b) z c) = Node R (Node B bl x a) y (balance_a b z (make_red c))

balance_d_r :: RBTree t -> t -> RBTree t -> RBTree t
balance_d_r a x (Node R b y c) = Node R a x (Node B b y c)
balance_d_r (Node B a x b) y bl = balance_a (Node R a x b) y bl
balance_d_r (Node R a x (Node B b y c)) z bl = Node R (balance_a (make_red a) x b) y (Node B c z bl)

make_red :: RBTree t -> RBTree t
make_red (Node B a x b) = Node R a x b
make_red t = t

move_subtree :: RBTree t -> RBTree t -> RBTree t
move_subtree Empty x = x
move_subtree x Empty = x
move_subtree (Node R a x b) (Node R c y d) =
  case move_subtree b c of
      Node R b' z c' -> Node R(Node R a x b') z (Node R c' y d)
      bc -> Node R a x (Node R bc y d)
move_subtree (Node B a x b) (Node B c y d) =
  case move_subtree b c of
      Node R b' z c' -> Node R(Node B a x b') z (Node B c' y d)
      bc -> balance_d_l a x (Node B bc y d)
move_subtree a (Node R b x c) = Node R (move_subtree a b) x c
move_subtree (Node R a x b) c = Node R a x (move_subtree b c)
