import RedBlackTree(RBTree(..), Color(..), size, color, height, append, delete, contains, from_list)

test_order :: (Ord t) => RBTree t -> Bool
test_order Empty = True
test_order (Node _ Empty _ Empty) = True
test_order (Node _ Empty v2 r@(Node _ _ v3 _)) = v2 < v3 && (test_order r)
test_order (Node _ l@(Node _ _ v1 _) v2 Empty) = v1 < v2 && (test_order l)
test_order (Node _ l@(Node _ _ v1 _) v2 r@(Node _ _ v3 _)) = v1 < v2 && v2 < v3 && (test_order l) && (test_order r)

test_black :: RBTree t -> Bool
test_black t = snd (_test_black t)
_test_black Empty = (0, True)
_test_black (Node c l v r) = let
  cur col
    | col == B = 1
    | otherwise = 0
  (cl, tl) = _test_black l
  (cr, tr) = _test_black r
  in (cl + cur c, tl && tr && cl == cr)

delete_some :: (Ord t) => RBTree t -> [t] -> RBTree t
delete_some t [] = t
delete_some t numbers = delete_some (delete (head numbers) t) (tail numbers)

test = do
  print $ test_order $ from_list [1..10]
  print $ test_black $ from_list [1..10]
  print $ test_order $ from_list [1..100]
  print $ test_black $ from_list [1..100]
  print $ test_order $ from_list [1..1000]
  print $ test_black $ from_list [1..1000]

  print $ height $ from_list [1, 10]
  print $ height $ from_list [1..100]
  print $ height $ from_list [1..1000]
  print $ height $ from_list [1..10000]
  print $ height $ from_list [1..100000]

  print $ height $ from_list [1..50]
  print $ height $ delete_some (from_list [1..50]) ([1..3])
  print $ height $ delete_some (from_list [1..50]) ([1..10])
  print $ height $ delete_some (from_list [1..50]) ([1..15])
  print $ height $ delete_some (from_list [1..50]) ([1..20])
  print $ height $ delete_some (from_list [1..50]) ([1..25])
  print $ height $ delete_some (from_list [1..50]) ([1..30])
  print $ height $ delete_some (from_list [1..50]) ([1..45])
  print $ height $ delete_some (from_list [1..50]) ([1..49])

  print $ test_order $ delete_some (from_list [1..50]) ([1..40])
  print $ test_order $ delete_some (from_list [1..50]) ([1..25])
  print $ test_order $ delete_some (from_list [1..50]) ([1..7])
