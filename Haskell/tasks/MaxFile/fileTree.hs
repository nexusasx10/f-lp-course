import System.IO (openFile, hFileSize, hClose, IOMode(ReadMode))
import System.Directory (getDirectoryContents, doesDirectoryExist)
import System.FilePath ((</>))
import Data.List ((\\))


remove_base :: [FilePath] -> [FilePath]
remove_base = (\\ [".", ".."])

remove_dirs :: [FilePath] -> IO [FilePath]
remove_dirs paths = remove paths where
  remove [] = return []
  remove paths = do
    directory <- doesDirectoryExist (head paths)
    if directory
      then
        remove (tail paths)
      else do
        t <- remove (tail paths)
        return ((head paths) : t)

list_of_files :: FilePath -> IO [FilePath]
list_of_files path =
  fmap (map (path </>) . remove_base) (getDirectoryContents path)

_walk_tree :: (Monad m) => (t -> m [t]) -> t -> m [t]
_walk_tree children root = do
  xs <- children root
  subChildren <- mapM (_walk_tree children) xs
  return $ root : concat subChildren

list_of_files_rec :: FilePath -> IO [FilePath]
list_of_files_rec = _walk_tree children
  where
    children path = do
      directory <- doesDirectoryExist path
      if directory
        then list_of_files path
        else return []

file_size :: FilePath -> IO Integer
file_size path = do
  fd <- openFile path ReadMode
  size <- hFileSize fd
  hClose fd
  return size

max_size_file :: FilePath -> IO FilePath
max_size_file path = do
  all_files <- list_of_files_rec path
  files <- remove_dirs all_files
  start_size <- file_size (head files)
  find_max (tail files) start_size (head files)

find_max :: [FilePath] -> Integer -> FilePath -> IO FilePath
find_max list max_ res = do
  if list == []
    then
      return res
    else do
      size <- file_size (head list)
      if size > max_
        then find_max (tail list) size (head list)
        else find_max (tail list) max_ res
