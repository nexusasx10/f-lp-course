-- data Maybe a = Just a | Nothing

-- data Either a b = Left a | Right b
{-# OPTIONS_GHC -XDeriveDataTypeable -XScopedTypeVariables #-}
import System.IO
import Control.Exception


fileSize::String -> IO (Maybe Integer)
fileSize path = handle (\(ex::IOException) -> return Nothing) $ do
  h <- openFile path ReadMode
  size <- hFileSize h
  hClose h
  return (Just size)
