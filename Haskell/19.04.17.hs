--zvon.org
minim::(Ord t)=>[t]->t
minim [] = error "Can not define"
minim [x] = x
minim (x:xs) = min x (minim xs)

addVectors::(Num t)=>(t, t)->(t, t)->(t, t)
addVectors a b = (fst a + fst b, snd a + snd b)

-- addVectors (x2, x2) (y1, y2) = (x1 + y1, x2 + y2)

prm_list::[Integer]
prm_list = [x | x <- [3, 5..], and[ x `mod` y /= 0 | y <- [3, 5.. ceiling ( sqrt (fromIntegral x))]]]
prm::Int->Integer
prm n = prm_list!!n
