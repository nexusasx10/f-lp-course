-- ghc, ghci
-- :? справка
-- quite выход
-- cd смена дирректории
-- :! command line
-- reload перекомпилировать
{--
...
--}
-- rem, mode остаток от деления, разное поведение на отрицательных

increment::Integer->Integer
increment = (+1)

myabs::Int->Int
myabs x = if x < 0
  then (-x)
  else x

mymax::Int->Int->Int
mymax x y
  | x > y = x
  | otherwise = y

heron1 a b c = sqrt(s*(s-a)*(s-b)*(s-c))
  where s = (a+b+c)/2

heron2 a b c = let s = (a+b+c)/2
  in sqrt(s*(s-a)*(s-b)*(s-c))

fibonachi n
  | n == 0 = 1
  | n == 1 = 1
  | otherwise = fibonachi (n-1) + fibonachi (n-2)
