{-# LANGUAGE #-}


module MTree (MultiTree(..), maxNodes, x)
where


data MultiTree a = Leaf | Node a [MultiTree a] deriving (Show)

x = Node 1 [Node 2 [Leaf], Node 3 [Leaf]]

maxNodes:: MultiTree a -> Int
maxNodes Leaf = 0
maxNodes (Node a l) = foldr max (length l) (map maxNodes l)


class Flatten i o where
  flatten::[i]->[o]

instance Flatten a a where
  flatten = id
