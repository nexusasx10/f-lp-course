{-# LANGUAGE DatatypeContexts #-}


module Struc (NestedList(Elem, List), Complex(..), inv, flatten) where

data Complex a = Complex a a deriving (Ord, Eq)

inv::(Num a) => Complex a -> Complex a
inv (Complex a b) = Complex a (-b)

myPrint::(Show a) => Complex a -> String
myPrint (Complex a b) = show a ++ " + " ++ show b ++ "i"

instance (Show a, Ord a, Num a) => Show (Complex a) where
  show (Complex a b)
    | b >= 0 = show a ++ " + " ++ show b ++ "i"
    | otherwise = show a ++ " " ++ show b ++ "i"

data NestedList a = Elem a | List [NestedList a]

flatten::NestedList a -> [a]
flatten (Elem a) = [a]
flatten (List a) = concatMap flatten a
